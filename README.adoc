---
:experimental:
---

== Password-store (pass) & Gopass fzf interface

An https://github.com/junegunn/fzf[fzf] based UI for
https://taskwarrior.org/[taskwarrior], implemented as a `/bin/sh`
script.

=== Install

1. Link / Copy / Move the file `passfzf` from this repo to your `$PATH`.
2. Make sure you have https://github.com/junegunn/fzf/releases[fzf 0.19.0 or
   newer] installed.

Make sure to have https://www.passwordstore.org/[pass] or
https://github.com/gopasspw/gopass[gopass] installed.

=== Usage

==== Command Line Arguments

`passfzf` accepts none. It launches an interface based on `(go)pass ls` and
fzf. And with some key bindings to let you do the most common operations upon
your secrets.

==== Key Bindings

`passfzf` uses https://www.mankier.com/1/fzf#Key_Bindings[`fzf`’s bindings] to
perform actions over the highlighted task or to change the list of tasks
presented. Since tasks are usually written in lower case letters, and in order
to mitigate key bindings clashes with a parent program `passfzf` may run inside
(e.g tmux, Neovim’s `:term` etc.), I decided to bind every upper case letter to
an action.

Pressing `?` prints a summary of all key bindings and their actions but
the lists here explain a bit more.

|===
|Shortcut |Purpose
|kbd:[Enter]
|Display (usually copy to clipboard) a secret (`pass show`)
|kbd:[F]
|Fully display (no clipboard) a secret (`pass show --force`)
|kbd:[C]
|Copy to clipboard a secret (`pass show --clip`)
|kbd:[O]
|Display (usually no clipboard) a TOTP/OTP code (`pass otp`)
|kbd:[T]
|Copy to clipboard a TOTP/OTP code (`pass otp --clip`)
|kbd:[E]
|Edit new or existing secrets (`pass edit`)
|kbd:[R]
|Edit recipient permissions (`pass reciepients`)
|kbd:[G]
|Generate a new password (`pass generate`)
|kbd:[N]
|Easy creation of new secrets (`pass create \| new`)
|kbd:[I]
|Insert a new secret (`pass insert`)
|kbd:[M]
|Move secrets from one location to another (`pass move <old-path> <new-path>`)
|kbd:[X]
|Remove secrets (`pass remove \| rm`)
|kbd:[H]
|Show password history (`pass history`)
|kbd:[CTRL-R]
|Reload list of secrets
|kbd:[S]
|Sync all local stores with their remotes (`pass sync`)
|kbd:[?]
|Show keys
|===

